import config

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, RadioField, IntegerField, \
    validators, ValidationError, TextAreaField


def forbidden_words_validation(form, data):
    forbidden_usernames = ['admin', 'settings', 'news', 'about', 'help', 'signin', 'signup',
                           'signout', 'terms', 'privacy', 'cookie', 'new', 'login', 'logout', 'administrator',
                           'join', 'account', 'username', 'root', 'blog', 'user', 'users', 'billing', 'subscribe',
                           'reviews', 'review', 'blog', 'edit', 'mail', 'email', 'home', 'job', 'jobs',
                           'contribute', 'newsletter', 'shop', 'profile', 'register', 'auth', 'authentication',
                           'campaign', 'config', 'delete', 'remove', 'forum', 'forums', 'download', 'downloads',
                           'contact', 'blogs', 'feed', 'faq', 'intranet', 'log', 'registration', 'search',
                           'explore', 'rss', 'support', 'status', 'static', 'media', 'setting', 'css', 'js',
                           'follow', 'activity', 'library', 'select', 'drop']
    if data.data.lower() in forbidden_usernames:
        raise ValidationError('Использовано зарезервированное слово: {}'.format(data.data))


def valid_username_validation(form, data):
    if '@' in data.data or '-' in data.data or '+' in data.data:
        raise ValidationError('В имени пользователя не должно встречаться следующих знаков: @, -, +.')


class LoginForm(FlaskForm):
    username = StringField('Имя пользователя', [
        validators.DataRequired(message='Поле "Логин" должно быть заполнено.'),
        forbidden_words_validation
    ])
    password = PasswordField('Пароль', [
        validators.DataRequired(message='Поле "Пароль" должно быть заполнено.')
    ])


class RegistrationForm(FlaskForm):
    username = StringField('Имя пользователя', [
        validators.DataRequired('Поле "Логин" должно быть заполнено.'),
        validators.Length(min=4, max=25, message="Логин должен быть не меньше 4-х и не более 25-ти знаков."),
        forbidden_words_validation,
        valid_username_validation,
    ])
    email = StringField('E-mail', [
        validators.DataRequired('Поле "E-mail" должно быть заполнено.'),
        validators.Email('Неправильно введен e-mail.'),
        validators.Length(min=6, max=320, message='Недопустимая длина E-mail.'),
    ])
    password = PasswordField('Пароль', [
        validators.DataRequired(message='Поле "Пароль" должно быть заполнено.'),
        validators.Length(min=6, max=40,
                          message='Пароль должен состоять не менее, чем из 6-ти и не более, чем из 40-ка знаков.'),
        validators.EqualTo('confirm', 'Пароли не совпадают.')
    ])
    confirm = PasswordField('Повторите пароль')
    age = IntegerField('Возраст', [
        validators.Optional(),
        validators.NumberRange(min=0, max=122,
                               message="Возраст должен находиться в диапазоне от 0 до 122.")
    ])
    sex = RadioField(label='Пол', validators=[
        validators.Optional()
    ],
        choices=[
            ('Мужской', 'Мужской'),
            ('Женский', 'Женский')
        ])
    secret_key = StringField('Учительский ключ')


class TaskForm(FlaskForm):
    task_input = TextAreaField('Код выполнения задания', [
        validators.DataRequired(message='Напишите решение задания.'),
    ])


class MakeTaskForm(FlaskForm):
    task_text = TextAreaField('Текст задания', [
        validators.DataRequired(message='У задания обязательно должна быть формулировка'),
        validators.Length(min=20, message='Минимальная длина текста задания - 20 символов.'),
    ])

    task_answer = TextAreaField('Правильный ответ', [
        validators.DataRequired(message='У задания обязательно должен быть правильный ответ.')
    ])

    task_db_choice = RadioField('Выбор базы данных',
                                choices=[(val, key) for key, val in list(config.SQL_DBS.items())[2:]])

    task_keywords = TextAreaField('Ключевые слова')