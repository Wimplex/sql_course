import re
import config
from db_process import connect_to_db, execute_query, Error


forbidden_words = ["drop", "alter", "use", "database"]
changing_words = ["insert", "delete", "update"]


def check(task, data):
    answ_tknzd = re.split(r'[ |,|\(|\)|;]+', data.lower())

    is_changing_word = False
    for word in changing_words:
        if word in answ_tknzd:
            is_changing_word = True

    if not is_changing_word and not data.lower().startswith('select'):
        return False, 'Код ответа должен начинаться с "SELECT".'

    if data.lower()[-1] != ';':
        return False, 'Код ответа должен заканчиваться точкой с запятой.'

    for keyword in task["keywords"]:
        if keyword.lower() not in answ_tknzd:
            return False, 'Ключевая конструкция "{}" должна присутствовать в запросе.'.format(keyword)

    for word in forbidden_words:
        if word.lower() in answ_tknzd:
            return False, 'В запросе использовано запрещенное слово "{}".'.format(word)

    if not is_changing_word:
        conn = connect_to_db(task["db"], config.SQL_HOST, config.SQL_ADMIN_USER, config.SQL_ADMIN_PASSWORD)
        try:
            task_query_outp = execute_query(conn, task['answer'])
        except Error as e:
            return False, 'Ошибка эталонного запроса: {}'.format(e[1])

        try:
            user_query_outp = execute_query(conn, data)
        except Error as e:
            return False, 'Ошибка запроса: {}'.format(e[1])

        if user_query_outp != task_query_outp:
            return False, ''
    else:
        if task['answer'].lower().replace("'", '"') != data.lower().replace("'", '"'):
            return False, ''
    return True, ''