import config           # файл конфигурации
import gc               # для сборки мусора
import os               # для просмотра директорий и работы с путями
import datetime         # для фиксирования даты регистрации пользователя
import json             # для загрузки заданий из файлов
import plotter          # для рисования статистики по выполненному заданию
import task_checker     # для проверки правильности выполнения задания
import forms            # для различных веб-форм

from flask import render_template, request, redirect, url_for, session, after_this_request
from app import app
from db_process import connect_to_db, execute_query, default_queries, NULL
from passlib.hash import sha256_crypt


@app.errorhandler(404)
def handle404(e):
    return render_template('flash.html', title='SQLCourse', main_text='404',
                           sub_text='Упс! Страница не найдена.', ref='/index/', ref_text='На главную',
                           logged_in=session.get('logged_in'), username=session.get('username'),
                           is_teacher=session.get('is_teacher'))


@app.route('/')
@app.route('/home/')
def index():
    return render_template('index.html', title='SQLCourse',
                           logged_in=session.get('logged_in'), username=session.get('username'),
                           is_teacher=session.get('is_teacher'))


@app.route('/databases/')
def databases():
    return render_template('data_bases.html', title='Базы данных - SQLCourse',
                           logged_in=session.get('logged_in'), username=session.get('username'),
                           is_teacher=session.get('is_teacher'))


@app.route('/lessons/')
def lessons():
    files = sorted([int(file.split('.')[0]) for file in os.listdir(config.LESSONS_DIR) if file.endswith('.json')])
    files = [str(file) + '.json' for file in files]
    res = []
    for file in files:
        cont = json.load(open(config.LESSONS_DIR + file, 'r', encoding='utf-8'))
        res.append({'index': file.split('.')[0], 'name': cont["name"]})

    return render_template('lessons.html', title='Уроки - SQLCourse', lessons=res,
                           logged_in=session.get('logged_in'), username=session.get('username'),
                           is_teacher=session.get('is_teacher'))


@app.route('/lessons/<lesson_num>')
def lesson(lesson_num):
    lesson_file_path = config.LESSONS_DIR + lesson_num + '.json'
    with open(lesson_file_path, encoding='utf-8') as file:
        lesson = json.load(file)
    return render_template('lesson.html', lesson_info=lesson, title=lesson['name'] + ' - SQLCourse',
                           logged_in=session.get('logged_in'), username=session.get('username'),
                           is_teacher=session.get('is_teacher'))

@app.route('/maketask/', methods=['GET', 'POST'])
def maketask():
    errors = []
    form = forms.MakeTaskForm(request.form)
    try:
        if form.validate_on_submit():
            task = {
                "task": form.task_text.data,
                "db": form.task_db_choice.data,
                "keywords": form.task_keywords.data.split(';'),
                "answer": form.task_answer.data
            }
            files = sorted([int(fname.split('.')[0]) for fname in os.listdir(config.TASKS_DIR) if fname.endswith('.json')])
            last = files[-1]
            with open('{}{}.json'.format(config.TASKS_DIR, last + 1), 'w', encoding='utf-8') as file:
                json.dump(task, file, ensure_ascii=False)

            return render_template('flash.html', title='SQLCourse', main_text='Задание сохранено',
                                   sub_text='Номер задания: {}'.format(last + 1), ref='/tasks/', ref_text='К списку заданий')

        else:
            errors += [". ".join(error[1]) for error in form.errors.items()]
    except Exception as e:
        errors += [e]
    return render_template('maketask.html', title='Создание задания - SQLCourse',
                           form=form, errors=errors, logged_in=session.get('logged_in'),
                           username=session.get('username'), is_teacher=session.get('is_teacher'))


@app.route('/tasks/')
def tasks():
    file_names = sorted([int(file.split('.')[0]) for file in os.listdir(config.TASKS_DIR) if file.endswith('.json')])
    file_names = [str(file) for file in file_names]
    file_names = [{'index': i, 'name': file} for i, file in enumerate(file_names, 1)]
    return render_template('tasks.html', title='Задания - SQLCourse', tasks=file_names,
                           logged_in=session.get('logged_in'), username=session.get('username'),
                           is_teacher=session.get('is_teacher'))


@app.route('/tasks/<tasknum>', methods=['GET', 'POST'])
def task(tasknum):
    task_file_path = config.TASKS_DIR + tasknum + '.json'
    with open(task_file_path, encoding='utf-8') as file:
        task = json.load(file)

    form = forms.TaskForm(request.form)
    errors = []
    try:
        if form.validate_on_submit():
            is_right = False
            check_result = task_checker.check(task, form.task_input.data)
            if check_result[0]:
                is_right = True

                # если юзер правильно решил задание, да еще и впервые,
                # то на него заводится карточка прогресса по этому заданию
                if session.get('username'):
                    con = connect_to_db(config.SQL_DBS["users"], config.SQL_HOST,
                                        config.SQL_ADMIN_USER, config.SQL_ADMIN_PASSWORD)
                    data = execute_query(
                        con,
                        default_queries["select_progress_using_values"].format(session['username'], tasknum)
                    )
                    if not data:
                        execute_query(
                            con,
                            default_queries["add_prog_card"].format(tasknum, session['username'])
                        )
            else:
                if check_result[1]:
                    errors += [check_result[1]]

            # создание статистики по заданию
            imgs = []
            if is_right:
                if session.get('logged_in'):
                    data = execute_query(con, default_queries["select_age_sex_by_tasknum"].format(tasknum))
                    imgs.append((
                        plotter.task_completings_by_age([age for age, _ in data]),
                        'Количество решений данного задания по возрасту пользователей.')
                    )
                    imgs.append((
                        plotter.task_completings_by_sex([sex for _, sex in data]),
                        'Количество решений данного задания по полу пользователей.')
                    )

                    if 'files' in session.keys():
                        session['files'] += [config.TEMP_DIR + img[0] for img in imgs]
            if not errors:
                return render_template('report.html', title="Отчет - SQLCourse",
                                       imgs=imgs, task_num=tasknum,
                                       is_right=is_right, logged_in=session.get('logged_in'),
                                       username=session.get('username'), is_teacher=session.get('is_teacher'))
                # до сих пор не допер, как удалять эти ссаные временные файлы!!!!!!!
                # try:
                #     for file in session.get('files'):
                #         os.remove(file)
                # except Exception as e:
                #     app.logger.error("Ошибка при удалении временных файлов", e)
        else:
            errors += [". ".join(error[1]) for error in form.errors.items()]
    except Exception as e:
        errors += [e]
    return render_template('task.html', title='Задание {} - SQLCourse'.format(tasknum),
                           errors=errors, task_num=tasknum, task_formulation=task["task"], form=form,
                           logged_in=session.get('logged_in'), username=session.get('username'),
                           is_teacher=session.get('is_teacher'))


@app.route('/login/', methods=['GET', 'POST'])
def login():
    errors = []
    form = forms.LoginForm(request.form)
    try:
        if form.validate_on_submit():
            conn = connect_to_db(config.SQL_DBS["users"], config.SQL_HOST,
                                 config.SQL_ADMIN_USER, config.SQL_ADMIN_PASSWORD)
            data = execute_query(
                conn,
                default_queries["select_user_using_login"].format(
                    form.username.data, form.password.data)
            )
            if data and sha256_crypt.verify(form.password.data, str(data[0][1])):
                session['logged_in'] = True
                session['username'] = form.username.data
                session['is_teacher'] = True if data[0][5] == 2 else False
                session['files'] = []
                return redirect(url_for('index'))
            else:
                errors += ["Неправильно введен логин или пароль."]
    except Exception as e:
        errors += [e]
    gc.collect()
    return render_template('login.html', title='Вход в систему - SQLCourse',
                           errors=errors, form=form)


@app.route('/log_out/')
def log_out():
    if 'files' in session.keys():
        for file in session['files']:
            os.remove(config.TEMP_DIR + file)
    session.clear()
    gc.collect()
    return redirect(url_for('index'))


@app.route('/register/', methods=['GET', 'POST'])
def register():
    form = forms.RegistrationForm(request.form)
    errors = []
    try:
        if form.validate_on_submit():
            status = 1

            # проверка занятости логина и емейла
            conn = connect_to_db(config.SQL_DBS["users"], config.SQL_HOST,
                                 config.SQL_ADMIN_USER, config.SQL_ADMIN_PASSWORD)
            login_check = execute_query(
                conn,
                default_queries["select_user_using_login"].format(form.username.data)
            )
            email_check = execute_query(
                conn,
                default_queries["select_user_using_email"].format(form.email.data)
            )

            if not login_check and not email_check:

                # проверка наличия учительского ключа в соответстующей бд
                if form.secret_key.data:
                    conn = connect_to_db(config.SQL_DBS["secret_keys"], config.SQL_HOST,
                                         config.SQL_ADMIN_USER, config.SQL_ADMIN_PASSWORD)
                    data = execute_query(conn,
                                         default_queries["check_keys"].format(form.secret_key.data))
                    print(data)
                    if data:
                        status = 2
                        execute_query(conn,
                                      default_queries["delete_key"].format(form.secret_key.data))
                    else:
                        errors += ["Учительский ключ введен неверно."]

                crypted_passw = sha256_crypt.encrypt(form.password.data)
                conn = connect_to_db(config.SQL_DBS["users"], config.SQL_HOST, config.SQL_ADMIN_USER,
                                     config.SQL_ADMIN_PASSWORD)
                data = execute_query(
                    conn,
                    default_queries["add_user"].format(
                        form.username.data,
                        crypted_passw,
                        form.email.data,
                        NULL if not form.age.data else form.age.data,
                        NULL if form.sex.data == 'None' else {"Мужской": 1, "Женский": 2}[form.sex.data],
                        status,
                        str(datetime.datetime.now().date())
                    )
                )
                return render_template('flash.html', title='SQLCourse', main_text='Вы успешно зарегистрировались',
                                       sub_text=form.username.data, ref='/login/', ref_text='Перейти к авторизации')
            elif login_check:
                errors += ["Этот логин уже используется."]
            elif email_check:
                errors += ["Этот e-mail уже используется."]
        else:
            errors += [". ".join(error[1]) for error in form.errors.items()]
    except Exception as e:
        errors += [e]
    return render_template('register.html', title='Регистрация - SQLCourse',
                           logged_in=session.get('logged_in'), username=session.get('username'),
                           errors=errors, form=form, is_teacher=session.get('is_teacher'))