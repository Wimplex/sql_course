import os

cwd = os.getcwd().replace('\\', '/')

CSRF_ENABLED = True
SECRET_KEY = "sd12k4lSsjf62aDh7h6jJ7kl4DwfdsHJ"

TASKS_DIR = cwd + '/data/tasks/'
LESSONS_DIR = cwd + '/data/lessons/'
TEMP_DIR = cwd + '/static/temp/'

SQL_ADMIN_USER = "sql_course_admin"
SQL_HOST = "localhost"
SQL_ADMIN_PASSWORD = "12345"
SQL_DB_USERS = "sql_course"
SQL_DB_SECRET_KEYS = "s_keys"

SQL_DBS = \
    {
        "users": "sql_course",
        "secret_keys": "s_keys",

        # training
        "bus_park": "bus_park",
        "library": "library"
    }