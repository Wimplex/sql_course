from MySQLdb import Error, connect, cursors, NULL

default_queries = {
    "select_user_using_login":
        'SELECT * FROM users WHERE login = "{}";',
    "select_user_using_email":
        'SELECT * FROM users WHERE email = "{}";',
    "add_user":
        'INSERT INTO users VALUES ("{}", "{}", "{}", {}, {}, {}, "{}");', # login, passw, email, age, sex, status, date
    "check_keys":
        'SELECT * FROM s_keys WHERE skey = "{}";',
    "delete_key":
        'DELETE FROM s_keys WHERE skey = "{}";',
    "select_users_ages":
        'SELECT age FROM users WHERE age IS NOT NULL',
    "select_progress_using_values":
        'SELECT * FROM users_prog WHERE login = "{}" AND num_of_task = {};',
    "add_prog_card":
        'INSERT INTO users_prog(num_of_task, login) VALUES ({}, "{}");',
    "select_age_sex_by_tasknum":
        'SELECT age, sex FROM users, users_prog WHERE users.login = users_prog.login AND users_prog.num_of_task = {} AND age IS NOT NULL AND sex IS NOT NULL;',
}


def connect_to_db(db_name, host, user, password):
    try:
        conn = connect(host=host,
                       user=user,
                       passwd=password,
                       db=db_name,
                       use_unicode=True,
                       charset="utf8")
    except Error as err:
        print("Ошибка соединения: {}".format(err))
        conn.close()
    conn.autocommit(on=True)
    return conn


def execute_query(connection, query):
    try:
        cur = connection.cursor(cursors.Cursor)
        cur.execute(query)
        data = cur.fetchall()
    except Error as err:
        print("Ошибка запроса: {}".format(err))
        return None
    return data