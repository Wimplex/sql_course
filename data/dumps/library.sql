-- MySQL dump 10.13  Distrib 5.5.23, for Win32 (x86)
--
-- Host: localhost    Database: library
-- ------------------------------------------------------
-- Server version	5.5.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `book_copy`
--

DROP TABLE IF EXISTS `book_copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_copy` (
  `cipher` varchar(7) NOT NULL,
  `num_of_room` int(2) NOT NULL,
  `isbn` varchar(14) NOT NULL,
  PRIMARY KEY (`cipher`),
  KEY `num_of_room` (`num_of_room`),
  KEY `isbn` (`isbn`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_copy`
--

LOCK TABLES `book_copy` WRITE;
/*!40000 ALTER TABLE `book_copy` DISABLE KEYS */;
INSERT INTO `book_copy` VALUES ('000-001',1,'ISBN-4235-3444'),('000-002',1,'ISBN-3298-7524'),('000-003',4,'ISBN-2846-5262'),('000-004',4,'ISBN-2846-5262'),('000-005',2,'ISBN-2428-7652'),('000-006',2,'ISBN-2983-7529'),('000-007',3,'ISBN-7893-2524'),('000-008',4,'ISBN-3984-7523'),('000-009',3,'ISBN-2305-7692'),('000-010',1,'ISBN-4235-3444'),('000-011',1,'ISBN-4235-3444'),('000-012',1,'ISBN-2394-0875'),('000-013',2,'ISBN-2983-7529'),('000-014',3,'ISBN-3453-4544'),('000-017',3,'ISBN-2305-7692'),('000-020',4,'ISBN-2385-8229');
/*!40000 ALTER TABLE `book_copy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_giving`
--

DROP TABLE IF EXISTS `book_giving`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_giving` (
  `giving_code` int(6) NOT NULL,
  `giving_date` date NOT NULL,
  `library_card_num` int(6) NOT NULL,
  `cipher` varchar(7) NOT NULL,
  PRIMARY KEY (`giving_code`),
  KEY `cipher` (`cipher`),
  KEY `library_card_num` (`library_card_num`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_giving`
--

LOCK TABLES `book_giving` WRITE;
/*!40000 ALTER TABLE `book_giving` DISABLE KEYS */;
INSERT INTO `book_giving` VALUES (1,'2013-03-04',200200,'000-005'),(2,'2017-10-13',100100,'000-006'),(3,'2014-12-11',900900,'000-003'),(4,'2016-08-12',100100,'000-012'),(5,'2016-09-01',200200,'000-010'),(6,'2016-09-01',100100,'000-011'),(7,'2015-09-01',500500,'000-001'),(8,'2011-05-08',700700,'000-013'),(9,'2004-07-21',300300,'000-014'),(10,'2012-11-12',200200,'000-008'),(11,'2016-01-31',600600,'000-017'),(12,'2015-06-12',600600,'000-020');
/*!40000 ALTER TABLE `book_giving` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books` (
  `isbn` varchar(14) NOT NULL,
  `name` varchar(50) NOT NULL,
  `year` int(4) DEFAULT NULL,
  `authors` varchar(30) NOT NULL,
  `num_of_copies` int(3) NOT NULL,
  `publishment` varchar(25) NOT NULL,
  PRIMARY KEY (`isbn`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES ('ISBN-2305-7692','Monday starts on Saturday',1990,'Arkadiy and Boris Strugatskiye',10,'Soyuz'),('ISBN-2323-4212','Process',1990,'Franz Kafka',4,'Rosmen'),('ISBN-2345-2345','Master and Margarita',1998,'Michail Bulgakov',10,'Piter'),('ISBN-2345-3334','Fathers and Sons',1980,'Ivan Turgenev',2,'Rosmen'),('ISBN-2356-7311','The Lower Depths',1992,'Maxim Gorkiy',33,'Rosmen'),('ISBN-2385-8229','Green mile',1999,'Steven King',11,'Rosmen'),('ISBN-2389-7520','V.Mayakovskiy. Sbornik stichov',1995,'V. Mayakovskiy',10,'Soyuz'),('ISBN-2394-0875','Data Science. Nauka o dannih s nulya',2017,'Daniel Grus',4,'BHV'),('ISBN-2428-7652','Cat book',2014,'A. Logunov',4,'Rosmen'),('ISBN-2846-5262','Norwegian forest',2010,'Haruki Murakami',14,'Rosmen'),('ISBN-2983-7529','Forsite\'s saga',1967,'John Golsuorsy',7,'Soyuz'),('ISBN-3298-7524','Rhymes and punches',2013,'E. Dmitrieva',2,'Rosmen'),('ISBN-3453-4544','Catcher in the rye',2001,'Jerome Salinger',5,'Rosmen'),('ISBN-3543-3411','Oblomov',2006,'Ivan Goncharov',15,'Rosmen'),('ISBN-3984-7523','Masha and the Bear',1910,'-',1,'The Book'),('ISBN-7893-2524','Rhyming and meaning making for dumbs',1990,'E. Letov',4,'Soyuz');
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `readers`
--

DROP TABLE IF EXISTS `readers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `readers` (
  `library_card_num` int(6) NOT NULL,
  `full_name` varchar(60) NOT NULL,
  `pass_num` varchar(11) NOT NULL,
  `birth_date` date NOT NULL,
  `education` varchar(9) NOT NULL,
  `ac_degree` varchar(3) NOT NULL,
  `num_of_room` int(2) NOT NULL,
  PRIMARY KEY (`library_card_num`),
  KEY `num_of_room` (`num_of_room`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `readers`
--

LOCK TABLES `readers` WRITE;
/*!40000 ALTER TABLE `readers` DISABLE KEYS */;
INSERT INTO `readers` VALUES (100100,'Logunov Alexey Andreevich','0810 111111','1997-03-09','Higher','No',1),(200200,'Dmitrieva Ekaterina Alekseevna','0810 222222','1998-06-18','Higher','No',2),(300300,'Lyulka Andrey Sergeevich','0810 333333','1998-01-17','Higher','No',1),(400400,'Kravtsov Nikolay','0810 444444','1998-06-14','Higher','No',3),(500500,'Derevtsov Denis Igorevich','0810 555555','1997-05-29','Higher','No',2),(600600,'Suhova Elizaveta','0811 342362','1997-06-18','Higher','No',2),(700700,'Dvachevskaya Alisa','0810 623948','1997-04-19','Middle','No',1),(800800,'Pupkin Vasiliy Vasilyevich','0811 012345','2004-09-30','Primary','Yes',3),(900900,'Letov Egor Fedorovich','0809 329857','1964-09-10','Higher','Yes',3);
/*!40000 ALTER TABLE `readers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reading_room`
--

DROP TABLE IF EXISTS `reading_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reading_room` (
  `num_of_room` int(2) NOT NULL,
  `name_of_room` varchar(20) NOT NULL,
  PRIMARY KEY (`num_of_room`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reading_room`
--

LOCK TABLES `reading_room` WRITE;
/*!40000 ALTER TABLE `reading_room` DISABLE KEYS */;
INSERT INTO `reading_room` VALUES (1,'Main'),(2,'Eastern'),(3,'Western'),(4,'Child\'s');
/*!40000 ALTER TABLE `reading_room` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-06  2:22:51
