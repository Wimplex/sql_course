-- MySQL dump 10.13  Distrib 5.5.23, for Win32 (x86)
--
-- Host: localhost    Database: Avtobusniy_park
-- ------------------------------------------------------
-- Server version	5.5.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `avtobus`
--

DROP TABLE IF EXISTS `avtobus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avtobus` (
  `Nomer_gosudarstvennoy_registracii` int(11) NOT NULL,
  `Pasportnye_dannye` varchar(12) NOT NULL,
  `Vmestimosty` int(3) NOT NULL,
  `Tip` varchar(20) NOT NULL,
  PRIMARY KEY (`Nomer_gosudarstvennoy_registracii`),
  KEY `Pasportnye_dannye` (`Pasportnye_dannye`),
  KEY `ind_nomer` (`Nomer_gosudarstvennoy_registracii`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avtobus`
--

LOCK TABLES `avtobus` WRITE;
/*!40000 ALTER TABLE `avtobus` DISABLE KEYS */;
INSERT INTO `avtobus` VALUES (1349793,'4013_429871',95,'Dizelniy'),(1568425,'4011_162897',120,'Benzinoviy'),(1648972,'4012_201364',90,'Dizelniy'),(1697884,'4102_734383',85,'Dizelniy'),(1787885,'4014_691989',100,'Dizelniy'),(1298674,'4011_349568',105,'Benzinoviy');
/*!40000 ALTER TABLE `avtobus` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `ins_bf` BEFORE INSERT ON `avtobus` FOR EACH ROW BEGIN
SET NEW.Nomer_gosudarstvennoy_registracii = NEW.Nomer_gosudarstvennoy_registracii;
SET NEW.Pasportnye_dannye = NEW.Pasportnye_dannye;
SET NEW.Vmestimosty = NEW.Vmestimosty;
SET NEW.Tip = LEFT(NEW.Tip,1);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `marshrut`
--

DROP TABLE IF EXISTS `marshrut`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marshrut` (
  `Nomer_marshruta` int(3) NOT NULL,
  `Intervali_dvizheniya` int(2) NOT NULL,
  `Nazvaniye_nachalnogo_punkta_dvizheniya` varchar(30) NOT NULL,
  `Nazvaniye_konechnogo_punkta_dvizheniya` varchar(30) NOT NULL,
  `Vremya_nachala_i_konca_dvizheniya` varchar(15) NOT NULL,
  `Protyazhennosty_v_minutah` int(3) NOT NULL,
  PRIMARY KEY (`Nomer_marshruta`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marshrut`
--

LOCK TABLES `marshrut` WRITE;
/*!40000 ALTER TABLE `marshrut` DISABLE KEYS */;
INSERT INTO `marshrut` VALUES (110,25,'St. metro \"Ladozhskaya\"','Narodnaya ul.','5:34-0:02',74),(118,40,'Ul. Kollontay','St. metro \"Lomonosovskaya\"','6:38-23:29',66),(15,10,'Starorusskaya ul.','Birzhevaya linya','5:22-0:16',56),(338,15,'St. metro \"Kupchino\"','Irinovskiy pr.','5:59-23:48',96),(12,12,'AS \"Podvoyskogo\"','Roshinskaya ul.','7:01-0:34',82);
/*!40000 ALTER TABLE `marshrut` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `polomka_avtobusa`
--

DROP TABLE IF EXISTS `polomka_avtobusa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `polomka_avtobusa` (
  `Pasportnye_dannye` varchar(12) NOT NULL,
  `Nomer_gosudarstvennoy_registracii` int(7) NOT NULL,
  `Nomer_marshruta` int(3) NOT NULL,
  `Prichina_polomki` varchar(100) NOT NULL,
  `Vremya_polomki` varchar(10) NOT NULL,
  `Mesto_polomki` varchar(25) NOT NULL,
  PRIMARY KEY (`Pasportnye_dannye`),
  KEY `Nomer_gosudarstvennoy_registracii` (`Nomer_gosudarstvennoy_registracii`),
  KEY `Nomer_marshruta` (`Nomer_marshruta`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `polomka_avtobusa`
--

LOCK TABLES `polomka_avtobusa` WRITE;
/*!40000 ALTER TABLE `polomka_avtobusa` DISABLE KEYS */;
INSERT INTO `polomka_avtobusa` VALUES ('4011_162897',1568425,110,'Neispravnosty_dvigatelya','8:38','Belorusskaya ul.'),('4012_201364',1648972,118,'Povrezhdeniye kolesa','15:03','Ul. Chudnovskogo'),('4013_429871',1349793,15,'Neispravnosty kompressora','19:48','Nevskiy pr.'),('4014_691989',1787885,338,'Otkaz tormozov','23:15','Pr. Bolshevikov'),('4102_734383',1697884,12,'Polomka styazhnih pruzhin kolodok','14:14','Pr. Solidarnosti');
/*!40000 ALTER TABLE `polomka_avtobusa` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `del_bf` BEFORE DELETE ON `polomka_avtobusa` FOR EACH ROW BEGIN
DELETE FROM voditely WHERE pasportnye_dannye=OLD.pasportnye_dannye;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Temporary table structure for view `predstavlenie`
--

DROP TABLE IF EXISTS `predstavlenie`;
/*!50001 DROP VIEW IF EXISTS `predstavlenie`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `predstavlenie` (
  `Pasportnye_dannye` varchar(12),
  `Surname_name_patronymic` varchar(50),
  `Stazh_raboty` int(11),
  `Oklad` int(11),
  `Nomer_marshruta` int(11)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `reys`
--

DROP TABLE IF EXISTS `reys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reys` (
  `Pasportnye_dannye` varchar(12) NOT NULL,
  `Nomer_gosudarstvennoy_registracii` int(7) NOT NULL,
  `Nomer_marshruta` int(3) NOT NULL,
  `Data_i_vremya_viezda` date NOT NULL,
  PRIMARY KEY (`Pasportnye_dannye`),
  KEY `Nomer_gosudarstvennoy_registracii` (`Nomer_gosudarstvennoy_registracii`),
  KEY `Nomer_marshruta` (`Nomer_marshruta`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reys`
--

LOCK TABLES `reys` WRITE;
/*!40000 ALTER TABLE `reys` DISABLE KEYS */;
INSERT INTO `reys` VALUES ('4011_162897',1568425,110,'2017-01-01'),('4012_201364',1648972,118,'2013-06-04'),('4014_691989',1787885,338,'2017-01-18'),('4102_734383',1697884,12,'2016-03-26'),('4011_349568',1298674,98,'2017-03-30');
/*!40000 ALTER TABLE `reys` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `ins_af` AFTER INSERT ON `reys` FOR EACH ROW BEGIN
INSERT INTO Reys(Pasportnye_dannye, Nomer_gosudarstvennoy_registracii, Nomer_marshruta, Data_i_vremya_viezda)
VALUES (NEW.Pasportnye_dannye, NEW.Nomer_gosudarstvennoy_registracii, NEW.Nomer_marshruta, NEW.Data_i_vremya_viezda);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `del_af` AFTER DELETE ON `reys` FOR EACH ROW BEGIN
DELETE FROM Reys WHERE Data_i_vremya_viezda = '2017-01-01';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `voditely`
--

DROP TABLE IF EXISTS `voditely`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voditely` (
  `Pasportnye_dannye` varchar(12) NOT NULL,
  `Surname_name_patronymic` varchar(50) NOT NULL,
  `Stazh_raboty` int(11) NOT NULL,
  `Oklad` int(11) NOT NULL,
  `Nomer_marshruta` int(11) DEFAULT NULL,
  PRIMARY KEY (`Pasportnye_dannye`),
  KEY `ind_pasp` (`Pasportnye_dannye`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voditely`
--

LOCK TABLES `voditely` WRITE;
/*!40000 ALTER TABLE `voditely` DISABLE KEYS */;
INSERT INTO `voditely` VALUES ('4011_162897','Legotin_Dmitriy_Vladimirovich',4,30500,110),('4012_201364','Radinovich_Evgeny_Sergeevich',12,30000,118),('4013_429871','Shishov_Semyon_Gennadyevich',8,30500,15),('4014_691989','Smirnov_Anton_Michailovich',20,40000,338),('4102_734383','Dmitriev_Alexey_Alexandrovich',15,35000,12),('4011_349568','Logunov_Alexey_Andreevich',16,48000,98);
/*!40000 ALTER TABLE `voditely` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `upd_bf` BEFORE UPDATE ON `voditely` FOR EACH ROW BEGIN
IF NEW.Oklad < 30000 THEN
SET NEW.Oklad = 30500;
ELSEIF NEW.Voditely.Oklad > 30000 AND NEW.Oklad < 50000 THEN
SET NEW.Oklad = 42000;
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `predstavlenie`
--

/*!50001 DROP TABLE IF EXISTS `predstavlenie`*/;
/*!50001 DROP VIEW IF EXISTS `predstavlenie`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `predstavlenie` AS select `voditely`.`Pasportnye_dannye` AS `Pasportnye_dannye`,`voditely`.`Surname_name_patronymic` AS `Surname_name_patronymic`,`voditely`.`Stazh_raboty` AS `Stazh_raboty`,`voditely`.`Oklad` AS `Oklad`,`voditely`.`Nomer_marshruta` AS `Nomer_marshruta` from `voditely` where (`voditely`.`Stazh_raboty` > 10) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-06  2:05:56
