import matplotlib.pyplot as plt
import datetime
import operator


from collections import Counter

def task_completings_by_age(data):
    ages_counts = Counter()
    for item in data:
        ages_counts[item] += 1

    ages_counts = sorted(ages_counts.items(), key=operator.itemgetter(0))
    xs = [age_count[0] for age_count in ages_counts]
    ys = [age_count[1] for age_count in ages_counts]

    fig, ax = plt.subplots(figsize=(10, 5))
    plt.xticks(xs)
    plt.yticks(ys)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.plot(xs, ys, color='blue')

    img_name = '{}.png'.format(datetime.datetime.now().time().microsecond)
    plt.savefig('static/temp/' + img_name, dpi=200)
    return img_name


def task_completings_by_sex(data):
    sex_counts = Counter()
    sex_mapping = {1: "Мужской", 2: "Женский"}
    for item in data:
        sex_counts[sex_mapping[item]] += 1

    fig, ax = plt.subplots(figsize=(10, 5))
    plt.xticks([1, 0], list(sex_counts.keys()))
    plt.yticks(list(sex_counts.values()))
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.bar(list(sex_counts.keys()), list(sex_counts.values()), width=0.7)

    img_name = '{}.png'.format(datetime.datetime.now().time().microsecond)
    plt.savefig('static/temp/' + img_name, dpi=200)
    return img_name
